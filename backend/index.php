<?php
	include("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html>
	<head>		
	  	<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="css/estilo.css">
		<link rel="stylesheet" type="text/css" href="bower/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="fancybox3/dist/jquery.fancybox.min.css">
		<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="fancybox3/dist/jquery.fancybox.min.js"></script>

		<!-- <link href="https://fonts.googleapis.com/css?family=Space+Mono" rel="stylesheet"> -->
		<!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->
		<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
		
		<title> BACKEND </title>
		<style type="text/css">
			body{
				padding: 0px;
				margin: 0px;
				/*font-family: "Raleway", "Helvetica Neue", Helvetica, Arial, sans-serif;*/
				/*font-family: 'Space Mono', monospace;*/
				/*font-family: 'Nunito', sans-serif;*/
				font-family: 'Josefin Sans', sans-serif;
			}			
		</style>
	</head>
	<body>
		<strong>Grabar texto de historia</strong>
		<form id="" action="../conexion/insertarTextoHistoria.php" method="POST">
			<label>titulo</label>
			<input type="text" name="titulo">
			<br>
			<label>texto</label>
			<input type="text" name="texto">
			<br>
			<button type="submit">Guardar</button>
		</form>

		<strong>Crear imagen para galeria</strong>
		
		<form id="" action="../conexion/insertarFotos.php" method="POST">

			<label>Codigo</label>
			<input type="text" name="codigo">
			<br>
			<label>Nombre</label>
			<input type="text" name="nombre">
			<br>
			<label>Ubicación</label>
			<input type="text" name="ubicacion">
			<br>
			<label>Descripción</label>
			<input type="textarea" name="descripcion">
			<br>
			<button type="submit">Enviar</button>
		</form>

		<strong>Imagenes de galeria</strong>
		<form id="" action="../conexion/insertarFotosPorGaleria.php" method="POST">
			<label>imagen</label>
			<input type="text" name="imagen">
			<br>
			<label>Galeria</label>
			<input type="text" name="galeria">
			<br>			
			<button type="submit">Enviar</button>
		</form>
		
		<strong>Texto de contacto</strong>
		<form id="" action="../conexion/insertarTextoContacto.php" method="POST">
			<label>Texto</label>
			<input type="text" name="texto">
			<br>
			<button type="submit">Guardar</button>
		</form>

	</body>
</html>	