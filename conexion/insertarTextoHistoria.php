<?php 

require 'conexion.php';

$titulo 	= $_POST["titulo"];
$texto 		= $_POST["texto"];

try{

	$sql = $base->prepare("INSERT INTO `historia`(`titulo`, `texto`) VALUES (:titulo, :texto)");

	$sql->bindParam(":titulo",	$titulo);
	$sql->bindParam(":texto",	$texto);
	
	$sql->execute();

}catch(Exception $e){
	die("error: ".$e->GetMessage());
}

?>