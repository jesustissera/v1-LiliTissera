<?php 

require 'conexion.php';

$ubicacion 		= $_POST["ubicacion"];
$descripcion 	= $_POST["descripcion"];
$codigo 		= $_POST["codigo"];
$nombre			= $_POST["nombre"];

try{

	$sql = $base->prepare("INSERT INTO `fotogaleria`(`ubicacion`, `descripcion`, `codigo`, `nombre`) VALUES (:ubicacion,:descripcion,:codigo,:nombre)");

	$sql->bindValue(":ubicacion",	$ubicacion);
	$sql->bindValue(":descripcion",	$descripcion);
	$sql->bindValue(":codigo",		$codigo);
	$sql->bindValue(":nombre",		$nombre);

	$sql->execute();

}catch(Exception $e){
	die("error: ".$e->GetMessage());
}


?>