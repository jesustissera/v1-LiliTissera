<?php 

require 'conexion.php';

$imagen 		= $_POST["imagen"];
$galeria 		= $_POST["galeria"];

try{

	$sql = $base->prepare("INSERT INTO `imagenesPorGaleria`(`imagen`, `idGaleria`) VALUES (:imagen, :idGaleria)");

	$sql->bindValue(":imagen",	$imagen);
	$sql->bindValue(":idGaleria",	$galeria);
	
	$sql->execute();

}catch(Exception $e){
	die("error: ".$e->GetMessage());
}

?>