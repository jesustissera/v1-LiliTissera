<?php

	include("conexion/conexion.php");
	include("conexion/traerFotos.php");
	include("conexion/traerHistoria.php");
	include("conexion/traerTextoContacto.php");

?>
<!DOCTYPE html>
<html>
	<head>		
	  	<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="css/estilo.css">
		<link rel="stylesheet" type="text/css" href="bower/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="fancybox3/dist/jquery.fancybox.min.css">
		<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="fancybox3/dist/jquery.fancybox.min.js"></script>

		<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
		
		<title> Prueba - lily </title>
		<style type="text/css">
			body{
				padding: 0px;
				margin: 0px;
				font-family: 'Josefin Sans', sans-serif;
			}			
		</style>
	</head>

	<body>
		<div class="header">
			<div class="h-box">
				<div class="container-img" >
					<a onclick="desplazarse('box1')">
						<img style="padding: 5px;width: 140px;" src="img/logo/logo.png">
					</a>	
				</div>
				<div class="menu">
					<ul >
						<li>
							<a id="boton" onclick="desplazarse('box5')" >
								<div class="menu-text">
								Presentación
								</div>
							</a>	
						</li>
						<li>
							<a onclick="desplazarse('box2')">
								<div class="menu-text">
								Galería
								</div>
							</a>
						</li>
						<li>
							<a onclick="desplazarse('box4')">
								<div class="menu-text">
								Contacto
								</div>
							</a>	
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div 	id="box1" 
				style="	width:50px;
						height:50px; 
						position:absolute;    
						left: 0;
				 		top:0;">
			
		</div>		
		<div  class="box1" style="position:relative">
				<img src="img/banner/banner2.png">
			<div id="box5" class="ancla" ></div>
		</div>
		
		<div class="box5" style="position:relative">
			<?php
			foreach ($historia as $key => $value) {
			?>
			<div class="titulo_1" style="font-weight:600">
				<?php echo $value["titulo"]?>
			</div>
			<div class="text" >
				<?php echo $value["texto"];?>
			</div>
			<?php
			}
			?>
			<div 	id="box2" class="ancla"></div>			
		</div>		

		<div class="box2" style="position:relative">
		<?php

			foreach ($resultado as $key => $value) {
				// echo $value['ubicacion'];
		?>			
			<a 	id="g_bloque<?php echo $key; ?>" 
				class="g-bloque" 
				data-caption="<?php echo $value['descripcion']; ?>" 
				href="img/foto-galeria/<?php echo $value['ubicacion']; ?>" 
				onmouseover='efectoImagen(<?php echo $key; ?>)' 
				onmouseout='quitarEfecto(<?php echo $key; ?>)' 
				data-fancybox="image-<?php echo $key;?>">

				<img id="img-bloque<?php echo $key; ?>" src="img/foto-galeria/<?php echo $value['ubicacion']; ?>" style="opacity:1">
				
				<span id="shadow<?php echo $key; ?>" class="shadow" style="display: none">
					<span id="" class="shadow-text">
						<?php echo $value['nombre']; ?>
					</span>				
				</span>	
			</a>

			<div style="display:none">
			<?php 

				$sql = $base->prepare("SELECT `imagen`, `idGaleria` FROM `imagenesporgaleria` WHERE `idGaleria` = :idGaleria");
				$sql->bindParam(":idGaleria",$value['id']);
				$sql->execute();
				$resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
				// var_dump($resultado);
				foreach ( $resultado as $k => $v ) {
			?>	
				<a 	href="img/foto-galeria/galeria/<?php echo $v["imagen"]; ?>" 
					data-fancybox="image-<?php echo $key;?>" 
					data-caption="<?php echo $value["descripcion"]; ?>">
				</a>	
			<?php
			}
			?>	
			</div>
		<?php
			// }
		}
		?>	
			<div 	id="box4" class="ancla"></div>			

		</div>		
		<div class="box4">
			
			<div class="col-md-6"  style="padding-right:0px;padding-left:0px">
				<div class="titulo_1" style="font-weight:600;">
					contacto
				</div>
				<div class="box-contacto">
				<?php
				foreach ($textocontacto as $key => $value) {
					echo $value["texto"];
				}
				?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="contacto_col_2">
					<form id="" >
						<div class="renglon">
							<input type="text" class="form-control" placeholder="Nombre" />
						</div>
						<div class="renglon">
							<input type="text" class="form-control" placeholder="Apellido">
						</div>
						<div class="renglon">
							<input type="text" class="form-control" placeholder="Email">
						</div>
						<div class="renglon">
							<textarea class="form-control" placeholder="Escribe tu mensaje..."></textarea>
						</div>
						<button class="btn btn-primary enviar" >Enviar</button>
					</form>
				</div>					
			</div>
		</div>
		<div class="footer">
			<div class="footer-sub">
			<div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
			</div>
		</div>		
	</body>
</html>
<script text="type/javascript">
	$(document).ready( function(){
		$('#g-bloque').hover( function(){
			$('#img-bloque').css('opacity','0.3');
			$('#shadow').css('display','block');
		}, function (){
			$('#img-bloque').css('opacity','1');
			$('#shadow').css('display','none');
		});
	});

	function efectoImagen(id){
		$('#img-bloque'+id).css('opacity','0.3');
		$('#shadow'+id).css('display','block');		
	}

	function quitarEfecto(id){
		$('#img-bloque'+id).css('opacity','1');
		$('#shadow'+id).css('display','none');
	}

	function desplazarse(id){
	    $('html,body').animate({
	        scrollTop: $("#"+id).offset().top
	    }, 2000);		
	}

	$("[data-fancybox]").fancybox({
		// Options will go here
	});

</script>